package movies.importer;

import java.util.ArrayList;

/*
 * This class extends from the Processor class and it takes an array 
 * list of strings and analyzes the data and arranges it into a new 
 * format which is year, title, runtime and the source respectively.
 * @author Kirill Parhomenco
 * */

public class ImdbImporter extends Processor{

    public ImdbImporter(String sourceDir, String outputDir) {
        super(sourceDir,outputDir,true);
    }

    @Override
    public ArrayList<String> process(ArrayList<String> input) { //22 entries on a line
        ArrayList<String> reformattedList = new ArrayList<String>();
        for (String s: input
             ) {
            // variable name is a tribute to globetrotters and mind switching in Futurama "The Prisoner of Benda" episode
            // using basketball variable as a temporary holder for full properties string
            String[] basketball = s.split("\\t", -1);
            if (basketball.length != 22) continue; // skipping if there is a field missing
            Movie tape = new Movie(basketball[3], basketball[1], basketball[6], "IMDB");
            reformattedList.add(tape.toString());
        }
        return reformattedList;
    }
}

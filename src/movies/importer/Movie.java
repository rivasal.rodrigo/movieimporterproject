package movies.importer;

import java.util.Objects;
/*
 * This class creates a movie object and contains
 * various getters to retrieve the information such as
 * release year, title, runtime and source.
 * @author Kirill Parhomenco and Rodrigo Rivas
 * */
public class Movie {
	private String releaseYear;
	private String movieTitle;
	private String movieRuntime;
	private String source;
	
	public Movie(String releaseYear, String movieTitle, String movieRuntime, String source){
		this.releaseYear = releaseYear;
		this.movieTitle = movieTitle;
		this.movieRuntime = movieRuntime;
		this.source = source;
	}
	
	public String getReleaseYear() {
		return releaseYear;
	}
	
	public String getMovieTitle() {
		return movieTitle;
	}
	
	public String getRuntime() {
		return movieRuntime;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String toString() {
		return getReleaseYear() + "\t" + getMovieTitle() +"\t" + getRuntime() +"\t" + getSource();
	}

	/*
	 * This method overrides the equals method. This is done to
	 * use the methods from the interface. 
	 * @author Kirill Parhomenco
	 * 
	 * */
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Movie movie = (Movie) o;
		return Objects.equals(releaseYear, movie.releaseYear) &&
				Objects.equals(movieTitle, movie.movieTitle) &&
				Objects.equals(movieRuntime, movie.movieRuntime);
	}

	@Override
	public int hashCode() {
		return Objects.hash(releaseYear, movieTitle, movieRuntime);
	}
}

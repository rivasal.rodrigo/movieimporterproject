package movies.importer;

import java.util.ArrayList;

/*
 * This class extends from the Processor class and it takes an array 
 * list of strings and analyzes the data and arranges it into a new 
 * format which is year, title, runtime and the source respectively.
 * @author Rodrigo Rivas
 * */

public class KaggleImporter extends Processor{
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,true);
	}
	
	public ArrayList<String> process(ArrayList<String> strarr){
		ArrayList<String> splitString = new ArrayList<String>();
		String[] word;
		
 		
		for(String i: strarr) {
			word = i.split("\\t");
			Movie j = new Movie(word[20],word[15],word[13],"Kaggle");
			splitString.add(j.toString());
		}
		
		return splitString;
	}
}

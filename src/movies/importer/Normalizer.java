package movies.importer;

import java.util.ArrayList;
/*
 * This class takes the previously processed array list
 * and normalize all the data to lower case and removes 
 * the strings from the runtime.
 * @author Rodrigo Rivas
 * */
public class Normalizer extends Processor{

	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,false);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> normalized = new ArrayList<String>();
		for(String i: input) {
			normalized.add(SentenceSplit(i));
		}
		
		return normalized;
	}
	
	/*
	 * This is a helper method to split a string
	 * when a tab happens within the string.
	 *
	 * */
	public String SentenceSplit(String sentence) {
		String [] splitting;
		splitting = sentence.split("\\t");
		Movie m = new Movie(splitting[0],splitting[1],regexVal(splitting[2]),splitting[3]);
		return m.toString().toLowerCase();
	}
	
	
	/*
	 * This method takes just the first word
	 * from a string when a space happens.
	 * */
	public String regexVal(String str) {
		String[] words = str.split("\\s");
		return words[0];	
	}
	
}

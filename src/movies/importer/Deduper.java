package movies.importer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * This class extends from the Processor class and it takes an array 
 * list of strings and analyzes the data. It makes sure that there
 * is not duplicate entries and makes use of HashMap to optimize the 
 * processing time.
 * @author Kirill Parhomenco and Rodrigo Rivas
 * */

public class Deduper extends Processor {
    public Deduper(String sourceDir, String outputDir) {
        super(sourceDir, outputDir, false);
    }

    @Override
    public ArrayList<String> process(ArrayList<String> input) {
        ArrayList<String> dedupedList = new ArrayList<>();
        HashMap<Movie, ArrayList<Movie>> movieMap = new HashMap<>(); //movie map to be converted to list at the end of deduping
        for (String s: input
             ) {
            String[] movieStrArr = s.split("\\t"); //getting entries for Movie instance
            Movie buster = new Movie(movieStrArr[0], movieStrArr[1], movieStrArr[2], movieStrArr[3]); //populating Movie
            ArrayList<Movie> busterAL = new ArrayList<>(); // to use with movieMap
            busterAL.add(buster); // self explanatory
            /*  next line puts the movie into hashmap if no previous entry, otherwise returns and stores
                the existing ArrayList<Movie> instance to the variable
             */
            ArrayList<Movie> existingAL = movieMap.putIfAbsent(buster, busterAL);
            if (existingAL != null){
                if (!existingAL.get(0).getSource().contains(buster.getSource())) {// if it has the same source(source is either the same or already changed to IMDB/RT), does nothing
                    // different sources - we change the source to combined IMDB/RT or RT/IMDB
                    existingAL.get(0).setSource(existingAL.get(0).getSource() + "/" + buster.getSource());
                }
            }
        } // loop finished populating the movieMap
        for (ArrayList<Movie> movieElement:
             movieMap.values()) {
            dedupedList.add(movieElement.get(0).toString());
        }
        return dedupedList;
    }
}

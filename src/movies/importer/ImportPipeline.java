package movies.importer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
/*
 * This is the main class. it creates new Processor array 
 * which takes files from different folders and then uses 
 * a helper method to executes the different classes to
 * process all the data into a same format.
 * @author Kirill Parhomenco and Rodrigo Rivas
 * */
public class ImportPipeline {
    public static void main(String[] args) throws IOException {
        Processor[] processArr = new Processor[5];
        
        processArr[0] = new ImdbImporter(
                "testfiles\\largefiles\\imdb",
                "testfiles\\largefiles\\afterImport");
        processArr[1] = new KaggleImporter(
                "testfiles\\largefiles\\kaggle",
                "testfiles\\largefiles\\afterImport");
        processArr[2] = new Normalizer(
                "testfiles\\largefiles\\afterImport",
                "testfiles\\largefiles\\afterNormalisation");
        processArr[3] = new Validator(
                "testfiles\\largefiles\\afterNormalisation",
                "testfiles\\largefiles\\afterValidation");
        processArr[4] = new Deduper("testfiles\\largefiles\\afterValidation",
                "testfiles\\largefiles\\afterDeduper");
        processAll(processArr);
     
        }

    /*
     * Helper method to execute each class that extends from the 
     * processor class.*/
    
    private static void processAll(Processor[] proc) throws IOException {
        for (Processor p :
                proc) {
            p.execute();
        }
    }
}

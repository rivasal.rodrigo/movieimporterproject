package movies.importer;

import java.util.ArrayList;

/*
 * This class takes the previously processed array list
 * and validates all the data is not null and converts 
 * the runtime to a number.
 * @author Kirill Parhomenco
 * */

public class Validator extends Processor {
    public Validator(String sourceDir, String outputDir) {
        super(sourceDir, outputDir, false);
    }

    @Override
    public ArrayList<String> process(ArrayList<String> input) {
        ArrayList<String> processedList = new ArrayList<String>();
        for (String s: input
             ) {
            if (passedValidation(s))
            processedList.add(s);
        }
        return processedList;
    }
    
    /*
	 * This is a helper method that splits a string
	 * when a tab happens within the string and verifies
	 * that the string is not null. It also converts
	 * the runtime from a string to number.
	 *
	 * */
    private boolean passedValidation(String s) throws NumberFormatException {
        String[] dissected = s.split("\\t");
        for (String str: dissected
             ) {
            if (str == null || str.equals("")) return false;
        }
        try {
            Integer.parseInt(dissected[0]);
            Integer.parseInt(dissected[2]);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}

package movies.tests;

import movies.importer.Validator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

	 /* Junit tests for the movie class ImdbImporter and Validator.
	 * @author Kirill Parhomenco*/
    @Test
    void processTest() {
        Validator bouncer = new Validator("dummyValue", "dummyValue2");
        ArrayList<String> testMovies = new ArrayList<String>();
        testMovies.add("1894\tMiss Jerry\t45\tIMDB");
        testMovies.add("Miss Jerry\t45\tIMDB");
        testMovies.add("1556\tMiss Jerry\t45\tIMDB");
        testMovies.add("1894\tMiss Jerry\t45 minutes\tIMDB");
        testMovies.add("long time ago\tMiss Jerry\t45\tIMDB");
        ArrayList<String> filtered = bouncer.process(testMovies);
        for (String s: filtered
             ) {
            String[] str = s.split("\\t", -1);
//            this should test if the strings are ok - testing the validation helper
            for (String k: str
                 ) {
                assertNotNull(k);
                assertNotEquals("", k);
            }
        }
    }
}
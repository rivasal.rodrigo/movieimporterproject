package movies.tests;

import movies.importer.ImdbImporter;
import movies.importer.Movie;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ImdbImporterTest {

/*
 * This test verify the IMDB importer class and compares
 * the result with the expected values which are the ones 
 * with the requirements.
 * @author Kirill Parhomenco*/

    @Test
    void processTest() {
        ImdbImporter monkey = new ImdbImporter("dummyValue", "dummyValue2");
        Movie testMovieHardcoded = new Movie("1894", "Miss Jerry", "45", "IMDB");
        ArrayList<String> testMovieFullInfo = new ArrayList<String>();
        testMovieFullInfo.add("tt0000009\tMiss Jerry\tMiss Jerry\t1894\t1894-10-09\tRomance\t45\tUSA\tNone\tAlexander Black\tAlexander Black\tAlexander Black Photoplays\t\"Blanche Bayliss, William Courtenay, Chauncey Depew\"\tThe adventures of a female reporter in the 1890s.\t5.9\t154\t\t\t\t\t1\t2\n");
        String testMovieTrimmedInfo = monkey.process(testMovieFullInfo).get(0);
        String[] tmfsi = testMovieFullInfo.get(0).split("\\t", -1); // stands for testMovieFullSplitInfo
        String[] tmtsi = testMovieTrimmedInfo.split("\\t", -1); // stands for testMovieTrimmedSplitInfo, just too long
        Movie testMovieComputed = new Movie(tmtsi[0], tmtsi[1], tmtsi[2], tmtsi[3]);
        assertEquals(22, tmfsi.length);
        assertEquals(4, tmtsi.length);
        assertEquals("1894", tmtsi[0]);
        assertEquals("Miss Jerry", tmtsi[1]);
        assertEquals("45", tmtsi[2]);
        assertEquals("IMDB", tmtsi[3]);
    }

}
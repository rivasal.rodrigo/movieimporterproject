package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;

class MoviesUnit {
/*
 * 
 * Junit tests for the movie class kaggleImporter and normalizer.
 * @author Rodrigo Rivas*/
	@Test
	void testCreateMovie() {
		Movie m1 = new Movie("2006","Borat","1h 26m","C:/Rodrigo");
		assertEquals("2006",m1.getReleaseYear());
		assertEquals("Borat",m1.getMovieTitle());
		assertEquals("1h 26m",m1.getRuntime());
		assertEquals("C:/Rodrigo",m1.getSource());
	}
	
	@Test
	void KaggleImporter() throws IOException {
		KaggleImporter mov = new KaggleImporter("","");
		Movie expected = new Movie("2008","The Mummy: Tomb of the Dragon Emperor","112 minutes","Kaggle");
		ArrayList<String> rawMovie = new ArrayList<String>();
		rawMovie.add("Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		String processedMovie = mov.process(rawMovie).get(0);
		String[] tempArr = processedMovie.split("\\t");
		Movie gotten = new Movie(tempArr[0],tempArr[1],tempArr[2],tempArr[3]);
		assertEquals(expected.getMovieTitle(),gotten.getMovieTitle());
		assertEquals(expected.getReleaseYear(),gotten.getReleaseYear());
		assertEquals(expected.getRuntime(),gotten.getRuntime());
		assertEquals(expected.getSource(),gotten.getSource());
		
	}
	
	@Test
	void KaggleNormalizer() throws IOException{
		/*This test will check that a string has been normalized. This means
		everything in lower case and the runtime without strings*/
		Normalizer norm = new Normalizer("","");
		Movie expected = new Movie("2008","the mummy: tomb of the dragon emperor","112","kaggle");
		ArrayList<String> rawMovie = new ArrayList<String>();
		rawMovie.add("2008	The Mummy: Tomb of the Dragon Emperor	112 minutes	Kaggle");
		String processedMovie = norm.process(rawMovie).get(0);
		String[] tempArr = processedMovie.split("\\t");
		Movie gotten = new Movie(tempArr[0],tempArr[1],tempArr[2],tempArr[3]);
		assertEquals(expected.getMovieTitle(),gotten.getMovieTitle());
		assertEquals(expected.getReleaseYear(),gotten.getReleaseYear());
		assertEquals(expected.getRuntime(),gotten.getRuntime());
		assertEquals(expected.getSource(),gotten.getSource());
	}

}
